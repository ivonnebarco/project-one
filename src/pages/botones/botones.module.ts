import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';
import { BotonesPage } from './botones';

@NgModule({
  declarations: [
    BotonesPage,
  ],
  imports: [
    IonicPageModule.forChild(BotonesPage),
  ],
  providers: [
    Toast
  ]
})
export class BotonesPageModule {}
