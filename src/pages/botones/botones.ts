import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

/**
 * Generated class for the BotonesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-botones',
  templateUrl: 'botones.html',
})
export class BotonesPage {

  ocultarcard: boolean = false;
  btn_saludo: boolean = true;
  btn_despedida: boolean;
  btn_calcular: boolean;
  btn_consultar: boolean;
  btn_comprar: boolean;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public myToastCtrl: ToastController) {
  }

  
  ionViewDidLoad() {
    console.log('ionViewDidLoad BotonesPage');
  }

  ocultarCard(){
    
    this.ocultarcard = !this.ocultarcard;
  }

  presentToast(btn_text: string) {
    const toast = this.myToastCtrl.create({
      message: '' + btn_text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }


  goToButtonsPage(){
    this.navCtrl.push(BotonesPage);
  } 

  
  

}
