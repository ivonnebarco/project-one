import { Component} from '@angular/core';
import { NavController, ToastController} from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';

import 'rxjs/add/operator/toPromise';
import { BotonesPage } from '../botones/botones';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  usuarios :any[];
  num :number;

  constructor(public navCtrl: NavController,
    public http:HttpProvider,
    public myToastCtrl: ToastController) {

  }


  // cargarUsuarios() extrae los datos json y los almacena en un array
  cargarUsuarios(){

    this.http.loadUsers(this.num).then(
      (res) => { 
        this.usuarios = res['results'];
        
      },
      (error) =>{
        console.error(error);

        let toast = this.myToastCtrl.create({
          message: 'Ups! Parece que no tienes conexión',
          duration: 3000,
          position: 'top'
        });
       
        toast.onDidDismiss(() => {
          console.log('toast Dismissed');
        });
       
        toast.present();

      }
    )
  }

  //presentMyToast() muestra un mensaje al hacer touch sobre cada usuario de la lista
  presentMyToast(nombreusuario: string) { 
    let toast = this.myToastCtrl.create({
      message: 'Hola mundo '+nombreusuario + '!',
      duration: 3000,
      position: 'top'
    });
   
    toast.onDidDismiss(() => {
      console.log('toast Dismissed');
    });
   
    toast.present();
  }

  goToButtonsPage(){
    this.navCtrl.push(BotonesPage);
  } 
  

}
